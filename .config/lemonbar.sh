#!/bin/sh

Clock() {
	DATETIME=$(date "+%a %b %d, %T")
	echo -n "$DATETIME"
}

while true; do

	echo "%{r} Coucou tout le monde! %{c} $(Clock) "
	sleep 1
done

